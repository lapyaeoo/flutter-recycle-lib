class RepaymentInfoModel {
  List<Data> data;
  Response response;

  RepaymentInfoModel({this.data, this.response});

  RepaymentInfoModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    if (this.response != null) {
      data['response'] = this.response.toJson();
    }
    return data;
  }
}

class Data {
  String customerName;
  String applicationNo;
  String customerGroupId;
  String customerId;
  String nrc;
  String period;
  String dueAmount;
  String dueDate;
  String reference;
  bool isSelected=false;

  Data(
      {this.customerName,
      this.applicationNo,
      this.customerGroupId,
      this.customerId,
      this.nrc,
      this.period,
      this.dueAmount,
        this.dueDate,
      this.reference,
      this.isSelected});

  Data.fromJson(Map<String, dynamic> json) {
    customerName = json['customer_name'].toString();
    applicationNo = json['application_no'].toString();
    customerGroupId = json['customer_group_id'].toString();
    customerId = json['customer_id'].toString();
    nrc = json['nrc'].toString();
    period = json['period'].toString();
    dueAmount = json['due_amount'].toString();
    dueDate =json['due_date'].toString();
    reference = json['reference'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customer_name'] = this.customerName;
    data['application_no'] = this.applicationNo;
    data['customer_group_id'] = this.customerGroupId;
    data['customer_id'] = this.customerId;
    data['nrc'] = this.nrc;
    data['period'] = this.period;
    data['due_amount'] = this.dueAmount;
    data['due_date'] = this.dueDate;
    data['reference'] = this.reference;
    return data;
  }
}

class Response {
  int code;
  String message;

  Response({this.code, this.message});

  Response.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['message'] = this.message;
    return data;
  }
}
