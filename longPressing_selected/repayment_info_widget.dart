import 'package:DEMO_APP/models/repayment_Info_model_blc.dart';
import 'package:DEMO_APP/views/repayment/repayment_data_view_blc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


// ignore: must_be_immutable
class RepaymentInfoBodyWidget extends StatefulWidget {
  List<Data>retrieveList;
  int index;
  RepaymentInfoBodyWidget(this.retrieveList, this.index);

  @override
  _RepaymentInfoBodyWidgetState createState() => _RepaymentInfoBodyWidgetState();
}

class _RepaymentInfoBodyWidgetState extends State<RepaymentInfoBodyWidget> {

  @override
  Widget build(BuildContext context) {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    return  Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            labelScribeTxt('Name: '),
            labelScribeTxt('LID: '),
            labelScribeTxt('Due Date: '),
            labelScribeTxt('Due Amount: '),
            labelScribeTxt('Period: '),
          ],
        ),
        SizedBox(
          width: 10.0,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            infScribeTxt(widget.retrieveList[widget.index]
                .customerName.toString()),
            infScribeTxt(widget.retrieveList[widget.index].reference.toString()),
            infScribeTxt(widget.retrieveList[widget.index].dueDate.toString()),
            infScribeTxt(widget.retrieveList[widget.index].dueAmount.toString()),
            infScribeTxt(widget.retrieveList[widget.index].period.toString()),
          ],
        )
      ],
    );
  }

  Widget labelScribeTxt(String label){
    return Text(label.toString(),
      style: TextStyle(
        fontSize: 14.0,
        height: 1.5,
      ),
    );
  }

  Widget infScribeTxt(String inforamtion){
    return Text(
      inforamtion.toString(),
      style: TextStyle(
        fontSize: 14.0,
        height: 1.5,
        color: Colors.black,
      ),
    );
  }
}
