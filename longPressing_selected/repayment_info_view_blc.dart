import 'package:DEMO_APP/models/repayment_Info_model_blc.dart';
import 'package:DEMO_APP/bloc/repayment_info_bloc_blc.dart';
import 'package:DEMO_APP/constants/ob/response_ob.dart';
import 'package:DEMO_APP/widgets/repayment_widgets/repayment_info_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:DEMO_APP/constants/theme.dart' as theme;
import '../../models/repayment_Info_model_blc.dart';
import 'repayment_data_view_blc.dart';

class RepaymentScreenBC extends StatefulWidget {
  @override
  _RepaymentScreenBCState createState() => _RepaymentScreenBCState();
}
class _RepaymentScreenBCState extends State<RepaymentScreenBC> {
  RepaymentInfoBlc _repaymentInfoBlc;
  TextEditingController searchFieldController = TextEditingController();
  String searchText='';
  ScanResult scanResult;
  List<Data> selectedItems=[];
  Color selectedColor=Colors.white;
  int selectedIndex=0;

  @override
  void initState() {
    // TODO: implement initState
    _repaymentInfoBlc = RepaymentInfoBlc();
    super.initState();
  }
  @override
  void dispose() {
    _repaymentInfoBlc.destroyRepayIn();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 12.0,left: 10.0),
                  width: 270,
                  height: 60,
                child:repaymentSearchField(),
              ),
              scanRepaymentBtn(),
            ],
          ),
          searchRepaymentBtn(),
          payRepaymentBtn(selectedItems),
          Expanded(
            child: StreamBuilder<ResponseOb>(
              stream: _repaymentInfoBlc.streamRepayment(),
              initialData:
                  ResponseOb(message: MsgState.loading, data: null),
              builder: (context, snapshot) {
                ResponseOb responseOb = snapshot.data;
                if (responseOb.message == MsgState.loading) {
                  return Center(
                    child: Text('...'),
                  );
                } else if (responseOb.message == MsgState.data) {
                  List<Data> retrieveApplicationInfoList = responseOb.data;
                  return ListView.builder(
                    itemCount: retrieveApplicationInfoList.length,
                    itemBuilder: (context, int index) {
                      return Card(
                        color: retrieveApplicationInfoList[index].isSelected?Colors.grey:Colors.white,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 2),
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                        ),
                        shadowColor: Colors.black,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: ListTile(
                                  onLongPress: (){
                                    setState(() {
                                      selectedItems.add(retrieveApplicationInfoList[index]);
                                      retrieveApplicationInfoList[index].isSelected=true;
                                      print(selectedItems.toString()+'add longPress++++++++++');

                                    });

                                  },
                                  onTap: (){
                                    if(retrieveApplicationInfoList.any((element) => element.isSelected)){
                                      setState(() {
                                        retrieveApplicationInfoList[index].isSelected=!retrieveApplicationInfoList[index].isSelected;
                                        if(!retrieveApplicationInfoList[index].isSelected){
                                          selectedItems.removeLast();
                                          print(selectedItems.toString()+'remove ontap++++++++++');
                                        }else{
                                          selectedItems.add(retrieveApplicationInfoList[index]);
                                          print(selectedItems.toString()+'add ontap++++++++++');
                                        }
                                      });
                                    }
                                  },
                                  subtitle: Column(
                                    children: [
                                      FittedBox(
                                        fit:BoxFit.contain,
                                        child: RepaymentInfoBodyWidget(retrieveApplicationInfoList,index),
                                      ),
                                    ],
                                  ),
                                  trailing: Padding(
                                    padding: const EdgeInsets.only(top: 20.0,),
                                    child: IconButton(
                                        icon: Icon(Icons.arrow_forward_ios),
                                        onPressed: (){
                                          Navigator.of(context).push(MaterialPageRoute(builder: (context){
                                            return Scaffold(
                                              appBar: AppBar(
                                                title: Text('REPAYMENT DATA',style:  new TextStyle(color: theme.accentColor, fontFamily: 'Dosis'),
                                                ),
                                                centerTitle: true,
                                              ),
                                              body: RepaymentDataView(retrieveList: retrieveApplicationInfoList,index: index),
                                            );
                                          }));
                                        }
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  );
                }
                else if(responseOb.message==MsgState.error){
                  return Container(
                    child: Center(
                      child: Text(responseOb.data.toString()),
                    ),
                  );
                }
                else {
                  return Container(
                    child: Center(
                      child: Text('Something wrong'),
                    ),
                  );
                }
              },
            ),
          ),
          SizedBox(
            height: 20.0,
          ),
        ],
      ),
    );
  }
  Widget scanRepaymentBtn() {
    return Container(

      child: IconButton(
        iconSize: 55.0,
          icon: Icon(Icons.border_horizontal),
          onPressed: (){
          setState(() {
            scanRepayment();
          });
          },
      ),
    );
  }
  Widget repaymentSearchField() {
    return TextField(
      controller: searchFieldController,
      decoration: InputDecoration(
        hintText: 'Search Application No/ Group No/ NRC',
        hintStyle: TextStyle(
          color: Colors.black45,
        ),
        isDense: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(5.0),
          ),
        ),
        filled: true,
        fillColor: Colors.white60,
        contentPadding: EdgeInsets.only(
            top: 15.0, bottom: 15.0, left: 15.0, right: 15.0),
      ),
    );
  }
  Widget searchRepaymentBtn() {
    return RaisedButton(
      onPressed: () {
        setState(() {
          txtFieldSearchRepayment();
        });

      },
      child: Text(
        'SEARCH',
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      color: Color(0xFF2A9F31),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
    );
  }
  Widget payRepaymentBtn(List<Data>selectedItems) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            right: 8.0,
          ),
          child: Container(
            height: 25.0,
            child: RaisedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context){
                  return Scaffold(
                    appBar: AppBar(
                      title: Text('REPAYMENT DATA',style:  new TextStyle(color: theme.accentColor, fontFamily: 'Dosis'),
                      ),
                      centerTitle: true,
                    ),
                    body: RepaymentDataView(retrieveList: selectedItems),
                  );
                }));
              },
              child: Text(
                'PAY',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              color: Color(0xFF298CFA),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
          ),
        ),
      ],
    );
  }


//------------------------------------------------------------------------------
  txtFieldSearchRepayment() {
    searchText = searchFieldController.text;
    _repaymentInfoBlc.getRetrieveInfoData(text: searchText);
    searchFieldController.clear();
  }

  Future scanRepayment() async{
      try{
        var result = await BarcodeScanner.scan();
        setState(() {
          scanResult=result;
          print(scanResult.rawContent.toString()+'+++++++++++++++++++++++++++++++++++++++');
          if(scanResult!=null){
            scanSearchRepayment();
          }

        });
      } on PlatformException catch(e){
        var result= ScanResult(
          type: ResultType.Error,
          format: BarcodeFormat.unknown,
        );
        if(e.code == BarcodeScanner.cameraAccessDenied){
          setState(() {
            result.rawContent='Camera was denined';
          });
        }else{
          result.rawContent= ' Something was wrong with $e';
        }
        setState(() {
          scanResult=result;
        });
      }
    }
    scanSearchRepayment(){
        _repaymentInfoBlc.getRetrieveInfoData(text: scanResult.rawContent.toString());
    }

    navigateRepaymentDataView(List<Data>list,{int index}){
      Navigator.of(context).push(MaterialPageRoute(builder: (context){
        return Scaffold(
          appBar: AppBar(
            title: Text('REPAYMENT DATA',style:  new TextStyle(color: theme.accentColor, fontFamily: 'Dosis'),
            ),
            centerTitle: true,
          ),
          body: RepaymentDataView(retrieveList: list,index: index,),
        );
      }));
    }
}