import 'dart:async';
import 'dart:convert';
import 'package:DEMO_APP/constants/network/base_network.dart';
import 'package:DEMO_APP/constants/ob/response_ob.dart';
import 'package:DEMO_APP/models/repayment_Info_model_blc.dart';
import 'package:rxdart/rxdart.dart';

class RepaymentInfoBlc extends BaseNetwork {
  PublishSubject<ResponseOb> _controllerRepayIn = PublishSubject();
  Stream<ResponseOb> streamRepayment() {
    return _controllerRepayIn.stream;
  }
    //0010 1301 1190 0253
  getRetrieveInfoData({String text}) {
    ResponseOb res = ResponseOb(message: MsgState.loading, data: 'loading');
    _controllerRepayIn.sink.add(res);

    getRequest(endUrl: text.toString()).then((receiveData) {
      if (receiveData.message == MsgState.data) {
        Map<String, dynamic> changedMap = json.decode(receiveData.data);
        if (changedMap['response']['code'] == 200) {
          RepaymentInfoModel repayIm = RepaymentInfoModel.fromJson(changedMap);
          res.message = MsgState.data;
          res.data = repayIm.data;
          _controllerRepayIn.sink.add(res);
        }
      }else{
        res.message = MsgState.error;
        res.data ='Something Wrong';
        _controllerRepayIn.sink.add(res);
      }
    });
  }

  void destroyRepayIn() {
    _controllerRepayIn.close();
  }
}
